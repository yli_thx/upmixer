function output = coherenceSeaparation(signal,fs,windowLength,hopSize,outputType,typeOfUpmix)

if outputType == "Binaural"
    
    % Selecting the string to retrieve the IR from the folder
    if(fs == 48000)
        sampleStr = '_48';
    elseif (fs == 44100)
        sampleStr = '_41';
    end
    
    if typeOfUpmix == 1
        ir1 = ['L_B',sampleStr,'.wav'];
        ir2 = ['R_B',sampleStr,'.wav'];
        ir3 = ['Ls_B',sampleStr,'.wav'];
        ir4 = ['Rs_B',sampleStr,'.wav'];
        
    elseif typeOfUpmix == 2
        
        ir1 = ['L_B',sampleStr,'.wav'];
        ir2 = ['R_B',sampleStr,'.wav'];
        ir3 = ['L_T',sampleStr,'.wav'];
        ir4 = ['R_T',sampleStr,'.wav'];
        
    elseif typeOfUpmix == 3
        
        ir1 = ['Ls_B',sampleStr,'.wav'];
        ir2 = ['Rs_B',sampleStr,'.wav'];
        ir3 = ['Ls_T',sampleStr,'.wav'];
        ir4 = ['Rs_T',sampleStr,'.wav'];
        
        
    end
    root = cd;
    cd IR
    
    % Readign the HRTFs
    firstImp = audioread(ir1);
    secondImp = audioread(ir2);
    thirdImp = audioread(ir3);
    fourthImp = audioread(ir4);
    
    % Make sure all impulses are the same size
    
    targetSize = min([length(firstImp) length(secondImp) length(firstImp) length(firstImp)]);
    irRange = (1024:32768)+1024; %1:targetSize; %% this is estimated from observing the IR files
    firstImp = firstImp(irRange,:);
    secondImp = secondImp(irRange,:);
    thirdImp = thirdImp(irRange,:);
    fourthImp = fourthImp(irRange,:);
    
    cd(root)
end

%% Actual UPMIX Code

% Window for time domain
wind  = 0.5*(1-cos(2*pi*(0:windowLength-1)/windowLength))';
%wind = hann(2048);

format long

% Extrating the coherence coefficients
% a = Inital zero vector to create the coeff buffers
a = zeros((windowLength/2 + 1),1);

% TODO: Improve the use of lambda 
% Current best value for lamda is this one because if I set it to one the
% algorithm doesn't work 
lambda = 0.99999;

% Get the amount of frames for this operation
amountOfFrames = ceil(length(signal)/hopSize);

% Init the buffer for the coefs
bufferCoefsX = complex(a,0);
bufferCoefsY = complex(a,0);
bufferCoefsXY = complex(a,0);

% Averaging number should be power of 2
avNum = 16;

if outputType == "Binaural"
    impulseLength = length(firstImp);
    
    convLength = windowLength + impulseLength - 1;
    impulsePad = zeros(convLength - impulseLength, 2);
    H1 = fft([firstImp; impulsePad]);
    H2 = fft([secondImp; impulsePad]);
    H3 = fft([thirdImp; impulsePad]);
    H4 = fft([fourthImp; impulsePad]);
    
    outputLength = size(signal,1) + impulseLength -1;
else
    outputLength = size(signal,1);
end

if outputType == "Binaural"
    output = zeros(outputLength,2);
elseif outputType == "Channels"
    output = zeros(outputLength,4);
end

for i=1:amountOfFrames-2

    beginning = ((i-1)*hopSize ) + 1;
    ending = beginning + windowLength - 1;    
        
    if ending > size(signal,1)
        ending = size(signal,1);
        windowLength = size(signal,1) - beginning +1;
    end
    
 % Upmix starts here    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% Coherence 
    X = fft(wind .* (signal(beginning:ending,1)));    
    Y = fft(wind .* (signal(beginning:ending,2)));
    X = X(1:windowLength/2 + 1);
    Y = Y(1:windowLength/2 + 1);
     
    Sxx = ((abs(X).^2) * (1-lambda)) + (bufferCoefsX * lambda);
    Syy = ((abs(Y).^2) * (1-lambda)) + (bufferCoefsY * lambda);
    Sxy = ((X .* conj(Y)) * (1-lambda)) + (bufferCoefsXY * lambda);
    
    % Here I get the coefficients for the whole spectrum 
    coefs = abs(Sxy) ./ sqrt(Sxx .* Syy);
     
    % average coeficients to broaden the bands  
    for h = 1:floor(windowLength/2)/avNum
        meanCoef = mean(coefs(h:h+avNum-1));
        coefs(h:h+avNum) = meanCoef;
    end
    
    % This are the actual upmixed vectors in frequency domain 
    aLFrame = X .* coefs;
    aRFrame = Y .* coefs;

    % I get the inverse of the mask
    invRat = 1- coefs;    

    bLFrame  =  X .* invRat;
    bRFrame =  Y .* invRat;
    
    % Save buffers
    bufferCoefsXY = Sxy;
    bufferCoefsX = Sxx;
    bufferCoefsY = Syy;
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %Upmix finishes here    
    
    if outputType == "Binaural"

    
        %% Binauralization starts 
        signalPad = zeros(convLength - windowLength,1);

        % We pass the frquency domain upmixed frames and we get back the time
        % domain binural stereo for that speaker/signal
        convolvedTime1 =  convolveFrame(aLFrame, H1, windowLength, signalPad);
        convolvedTime2 =  convolveFrame(aRFrame, H2, windowLength, signalPad);
        convolvedTime3 =  convolveFrame(bLFrame, H3, windowLength, signalPad);
        convolvedTime4 =  convolveFrame(bRFrame, H4, windowLength, signalPad);

        convolvedSegment = convolvedTime1 + convolvedTime2 + convolvedTime3 + convolvedTime4;

        endIxConv = ending + impulseLength -1;

        % Store data in output vector 
        output(beginning:endIxConv,:) = output(beginning:endIxConv,:) + convolvedSegment; 
    
    elseif outputType == "Channels"
        
        output(beginning:ending,1) = output(beginning:ending,1) + toTime(aLFrame,windowLength);
        output(beginning:ending,2) = output(beginning:ending,2) + toTime(aRFrame,windowLength);
        output(beginning:ending,3) = output(beginning:ending,3) + toTime(bLFrame,windowLength);
        output(beginning:ending,4) = output(beginning:ending,4) + toTime(bRFrame,windowLength);
     
    end
end

end

% This is the funtion to spatialize, not to upmix
function convolvedSegment = convolveFrame(frame, H, windowLength, signalPad)
% This is a very ineficiicint way to spatialize as I'm doing two ffts and two iffts, but
% this is just to showcase how it would sound spatilaized. You will have to
% wait way longer if you want the binaural output 

    aFull = [frame;flip(conj(frame(2:windowLength/2)))];
    aPad = [ifft(aFull,windowLength) ,ifft(aFull,windowLength) ; signalPad,signalPad];
    VOICES1 = fft(aPad);
    FASTCONV1 = VOICES1 .* H;
    convolvedSegment = ifft(FASTCONV1);
end

function timeSegment = toTime(frame,windowLength)
    aFull = [frame;flip(conj(frame(2:windowLength/2)))];
    timeSegment = ifft(aFull,windowLength);
end
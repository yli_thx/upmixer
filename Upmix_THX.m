
%% Upmix_THX
% By: Simon Calle
% Property of THX, Ltd.


%signal: The name of the audio you want to process

% outputType: You need to pass a string. "Binaural" will output a stereo
% wav file with a binaural rendering. "Channels" will output the separate
% 4 mono channels.

% binauralType:
% 1 = IN: Stereo
%     OUT: Binaural[L - R -Ls - Rs]
% 2 = IN: Stereo
%     OUT: binaural [L - R - TopL - TopR]
% 3 = IN: Stereo
%     OUT = Binaural [Ls - Rs - TopLs - TopRs]

function output = Upmix_THX(signal, outputType, binauralType, windowLength, fs)

if nargin < 1
    signal = 'GuitPian.wav';
end

if nargin < 2
    outputType = 'Binaural';
end

if nargin < 3
    binauralType = 1;
end

if nargin < 4
    windowLength = 1024;
end

% Inital settingsfor real-time implementation
hopSize = windowLength / 2;

% Read the signal, it can be a stereo , 5.1, 7.1
if isa(signal, 'string') || isa(signal, 'char')
    [signal,fs] = audioread(signal);
end

output = coherenceSeaparation(signal,fs,windowLength,hopSize,outputType,binauralType);

if outputType == "Binaural"
    if nargout == 0
        audiowrite('.output/output.wav',output(windowLength*3:end,:),fs);
    else
        output = output(windowLength*3:end,:);
    end
elseif outputType == "Channels"
    if nargout == 0
        audiowrite('.output/Channel1.wav',output(windowLength*3:end,1),fs);
        audiowrite('.output/Channel2.wav',output(windowLength*3:end,2),fs);
        audiowrite('.output/Channel3.wav',output(windowLength*3:end,3),fs);
        audiowrite('.output/Channel4.wav',output(windowLength*3:end,4),fs);
    else
        output = output(:,:);
    end
end
    
end

% extract two ambience channels
function [output, state] = simple_upmix(stereo, state)

if isa(stereo, 'string') || isa(stereo, 'char')
    
    %% process a file
    [st, fs] = audioread(stereo, [1 48000*60]);
    
    y = simple_upmix(st);
    
    audiowrite('.output/simple_upmix.wav', y, fs);
    
elseif nargin == 1    
    
    hop_size = 256;
    assert(size(stereo, 2) == 2); % must be stereo
    state = [];
    
    output = zeros(size(stereo));
    
    offset = 1;
    while (offset + hop_size) < size(stereo, 1)
        [output(offset:offset+hop_size-1, :), state] = simple_upmix(stereo(offset:offset+hop_size-1,:), state);
        offset = offset + hop_size;
    end
    
else
    
    if isempty(state)
        
        %% decorrelator
        N = 40;
        [state.l_decor.b, state.l_decor.a] = DecAllPass(N);
        state.l_decor.z = zeros(N, 1);
        N = 60;
        [state.r_decor.b, state.r_decor.a] = DecAllPass(N);
        state.r_decor.z = zeros(N, 1);
        
        %% PBA HPF 1k
        [state.hpf.b, state.hpf.a] = butter(2, 1000 / (48000 / 2), 'high');
        state.l_hpf.z = zeros(2, 1);
        state.r_hpf.z = zeros(2, 1);
        
        %% LMS
        global simple_upmix_option;
        if isempty(simple_upmix_option)
            state.option = 'lms'; % or passive
        else
            state.option = simple_upmix_option;
        end
        state.lms.wl = 1;
        state.lms.wr = 1;
        state.lms.mu = 0.0001;
        state.lms.r = [];
        state.lms.vw = [];
    end
    
    l = stereo(:, 1);
    r = stereo(:, 2);
    
    if state.option == 'lms'
        
        %{
        Upmixing and Downmixing Two-channel Stereo Audio for Consumer Electronics 
        Mingsian R. Bai and Geng-Yu Shih
        IEEE Transactions on Consumer Electronics, Vol. 53, No. 3, AUGUST 2007
        Section II.A
        https://ir.nctu.edu.tw/bitstream/11536/6334/1/000249541300030.pdf
        %}
        
        N = size(stereo, 1);
        
        %{
        rxy = dot(l, r);
        rx = dot(l, l);        
        ry = dot(r, r);
        corr = rxy / (rx * ry) ^ 0.5;
        %}
        
        % EQ(7)
        corr = abs(dot(l, r)) / sum(abs(l .* r));
        
        for n=1:N           
            [ll, state.lms.wl] = lms1(r(n), l(n), state.lms.wl, corr);
            [rr, state.lms.wr] = lms1(l(n), r(n), state.lms.wr, corr);
            
            l(n) = ll;
            r(n) = rr;
        end
        
        state.lms.r = [state.lms.r corr];
        state.lms.vw = [state.lms.vw state.lms.wl];
        
    else
        
        % side to ls and rs
        m = l + r;
        l = l - m * 0.5;
        r = r - m * 0.5;
        
    end
    
    [yl, state.l_decor.z] = filter(state.l_decor.b, state.l_decor.a, l, state.l_decor.z);
    [yr, state.r_decor.z] = filter(state.r_decor.b, state.r_decor.a, r, state.r_decor.z);    
    
    [yl, state.l_hpf.z] = filter(state.hpf.b, state.hpf.a, yl, state.l_hpf.z);
    [yr, state.r_hpf.z] = filter(state.hpf.b, state.hpf.a, yr, state.r_hpf.z);
    
    output = [yl yr];

    %disp(max(output));
end

end

% scaler LMS
function [e, w] = lms1(x, d, w, rho)
    mu = 0.001;
    y = w * x;
    e = d - y;
    w = w + mu * rho / (x * x + 0.001) * e * x;
end


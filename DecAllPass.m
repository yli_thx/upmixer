%% Decorrelation FIlter based on Potard’s recommendation
%% IIR Decorrelation filter (all−pass)
%% Stereo-to-Five Channels Upmix Methods by P PAPASTERGIOU 2018
function [polb, pola, A, B, wavout] = DecAllPass(N, wav) % N is filter order−even number

A = rand(N/2, 1) * 0.9; %
B= (rand(N/2, 1 ) - 0.5) * 2 * pi; % random phase

% make complex numbers
[real, imag] = pol2cart(B, A) ;
compli = real + 1i * imag ;
% second part is complex conjugates roots
compli((N/2)+1:N) = conj(compli);

% make denominator polynomial
pola = poly(compli);
% make numerator polynomial
% coefficients in reverse order to get all-pass response
polb = pola (length(pola ) : -1 : 1 ) ;

% filter input signal
if nargin == 2
    wavout = filter(polb, pola, wav);
end
function main()

options = ["no-height", "simon-1024", "simon-512", "simon-256", "passive", "lms"];
%options = ["simon-1024"];

options = options(randperm(length(options)));

files = dir(".data/*.wav");

d = string(datetime)
d = strrep(d,' ','_');
d = strrep(d,':','-');
output_dir = ".output_" + d;
list_file_name = ".output_" + d + ".txt";
mkdir(output_dir)

list_file = fopen(list_file_name, 'w');
for n = 1:length(options)
    fprintf(list_file, '%d, %s\n', n, options(n));
end
fclose(list_file);

try
    for m = 1:length(files)
        file_name = fullfile(files(m).folder, files(m).name);
        info = audioinfo(file_name);
        test_length = min(info.Duration, 120) * info.SampleRate;
        [signal, fs] = audioread(file_name, [1 test_length]);
        hrir = load_ir(fs);
        for n = 1:length(options)
            upmixxed = upmix(signal, fs, options(n));
            if isempty(upmixxed)
                continue
            end
            downmixed = downmix(upmixxed, hrir);
            suffix = sprintf("-downmix-%d.wav", n);
            out_file_name = strrep(file_name, ".wav", suffix);
            out_file_name = strrep(out_file_name, ".data", output_dir);
            disp(out_file_name);
            audiowrite(out_file_name, downmixed, fs);
        end    
    end
catch ME
    disp(ME.stack(1));
end

end

%% input is 5.1.0, output is 5.1.4
function output = upmix(signal, fs, option)
    [ns, nc] = size(signal);
    assert(nc == 6);
    output = [];
    if (option == "no-height")
        output = zeros(ns, 10);
        output(:, 1:6) = signal;
    elseif option == "simon-1024"
        output = upmix_simon(signal, 1024, fs);
    elseif option == "simon-512"
        output = upmix_simon(signal, 512, fs);
    elseif option == "simon-256"
        output = upmix_simon(signal, 256, fs);
    elseif option == "passive"
        global simple_upmix_option;
        simple_upmix_option = "passive";
        output = zeros(ns, 10);
        output(:, 1:6) = signal;
        output(:, 7:8) = simple_upmix(signal(:,1:2));
        output(:, 9:10) = simple_upmix(signal(:,5:6));        
    elseif option == "lms"
        global simple_upmix_option;
        simple_upmix_option = "lms";
        output = zeros(ns, 10);
        output(:, 1:6) = signal;
        output(:, 7:8) = simple_upmix(signal(:,1:2));
        output(:, 9:10) = simple_upmix(signal(:,5:6));
    end
end

function output = upmix_simon(input, win_size, fs)
    [ns, nc] = size(input);
    output = zeros(ns, 10);
    f4 = Upmix_THX(input(:,1:2), "Channels", 1, win_size, fs);
    b4 = Upmix_THX(input(:,5:6), "Channels", 1, win_size, fs);
    output(:, 1:2) = f4(:, 1:2);
    output(:, 3:4) = input(:, 3:4);
    output(:, 5:6) = b4(:, 1:2);
    output(:, 7:8) = f4(:, 3:4);
    output(:, 9:10) = b4(:, 3:4);
end

%% 
function out = match_size(input, ns)
    [ni, nc] = size(input, 1);
    if ni > ns
        out = input(1:ns, :);
    else
        out = zeros(ns, nc);
        out(1:ni, :) = input;
    end       
end

%% binaural downmix from 5.1.4 to stereo
function output = downmix(input, hrir)
    [ns, nc] = size(input);
    l = fftfilt(hrir(:, 1), input(:, 1));
    r = fftfilt(hrir(:, 2), input(:, 1));
    for n = 2:nc
        l = l + fftfilt(hrir(:, n * 2 - 1), input(:, n));
        r = r + fftfilt(hrir(:, n * 2    ), input(:, n));
    end
    output = [l r];
    peak = max(max(abs(output)));
    output = output / peak * 0.9;
end

%% load 9 pairs of channel HRIRs L(HRIR-L/HRHR-R)/R/C/LFE/LS/RS/LT/RT/LST/RST
function hrir = load_ir(fs)

if(fs == 48000)
    sampleStr = '_48.wav';     
elseif (fs == 44100)    
    sampleStr = '_41.wav';
end

names = ["L_B", "R_B", "C_B", "LFE_B", "Ls_B", "Rs_B", "L_T", "R_T", "Ls_T", "Rs_T"];
num_chan = length(names);

min_length = 100000;

for n=1:num_chan
    info = audioinfo("ir514/" + names(n) + sampleStr);
    min_length = min(min_length, info.TotalSamples);
end

hrir = zeros(min_length, num_chan * 2);

for n=1:num_chan
    hrir(:, (n*2-1):n*2) = audioread("ir514/" + names(n) + sampleStr, [1, min_length]);
end

end
